import {useRef, useState} from 'react';

enum Operators {
  sum,
  subtraction,
  multiply,
  divide,
}

export const useCalculator = () => {
  const [previousNumber, setPreviousNumber] = useState('0');
  const [number, setNumber] = useState('0');

  const lastOperation = useRef<Operators>();

  const clean = () => {
    setNumber('0');
    setPreviousNumber('0');
  };

  const buildNumber = (textNumber: string) => {
    // Not accepted double point
    if (number.includes('.') && textNumber === '.') {
      return;
    }

    if (number.startsWith('0') || number.startsWith('-0')) {
      // decimal pointer
      if (textNumber === '.') {
        setNumber(number + textNumber);
        // check if exists the value cero and exists one pointer
      } else if (textNumber === '0' && number.includes('.')) {
        setNumber(number + textNumber);
        // check if not exists the value cero and not include one pointer
      } else if (textNumber !== '0' && !number.includes('.')) {
        setNumber(textNumber);
        // avoid 0000.0
      } else if (textNumber === '0' && !number.includes('.')) {
        setNumber(number);
      }
    } else {
      setNumber(number + textNumber);
    }
  };

  const positiveNegative = () => {
    if (number.includes('-')) {
      setNumber(number.replace('-', ''));
    } else {
      setNumber('-' + number);
    }
  };

  const btnDelete = () => {
    let negative = '';
    let numberTemp = number;

    if (number.includes('-')) {
      negative = '-';
      numberTemp = number.substr(1);
    }

    if (numberTemp.length > 1) {
      setNumber(negative + numberTemp.slice(0, -1));
    } else {
      setNumber('0');
    }
  };

  const changePreviousNumber = () => {
    if (number.endsWith('.')) {
      setPreviousNumber(number.slice(0, -1));
    } else {
      setPreviousNumber(number);
    }
    setNumber('0');
  };

  const btnDivide = () => {
    changePreviousNumber();
    lastOperation.current = Operators.divide;
  };

  const btnMultiply = () => {
    changePreviousNumber();
    lastOperation.current = Operators.multiply;
  };

  const btnSubtraction = () => {
    changePreviousNumber();
    lastOperation.current = Operators.subtraction;
  };

  const btnSum = () => {
    changePreviousNumber();
    lastOperation.current = Operators.sum;
  };

  const calculate = () => {
    const num1 = Number(number);
    const num2 = Number(previousNumber);

    switch (lastOperation.current) {
      case Operators.sum:
        setNumber(`${num1 + num2}`);
        break;
      case Operators.subtraction:
        setNumber(`${num2 - num1}`);
        break;
      case Operators.multiply:
        setNumber(`${num1 * num2}`);
        break;
      case Operators.divide:
        setNumber(`${num2 / num1}`);
        break;
    }

    setPreviousNumber('0');
  };

  return {
    previousNumber,
    number,
    clean,
    buildNumber,
    positiveNegative,
    btnDelete,
    btnDivide,
    btnMultiply,
    btnSubtraction,
    btnSum,
    calculate,
  };
};
